//
//  ViewController.swift
//  Sanket_Panchal_21_May
//
//  Created by Unity on 5/21/21.
//  Copyright © 2021 Unity. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var arrayData : [Result]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //        callAPi(artist: "arijit")
        tableView.tableFooterView = UIView()
    }
    
   
    
    
    func callAPi(artist: String) {
        
        if artist == "" { return }
        DispatchQueue.global().async { //(https://itunes.apple.com/search?term=arijit)
            // Details using this api
//            (https://itunes.apple.com/lookup?id=909253)
            
            guard let url = URL(string: "https://itunes.apple.com/search?term=\(artist)") else { return  }
            
            let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                
                if error != nil {
                    print(error?.localizedDescription as Any)
                    alertView(msg: error?.localizedDescription ?? "", vc: self)
                    return
                }
                
                do {
                    DispatchQueue.main.async {
                        do {
                            let res = try? JSONDecoder().decode(AlbumModel.self, from: data!)
                            
                            if res?.resultCount ?? 0 == 0 {
                                alertView(msg: "No result Found", vc: self)
                                return
                            }
                            
                            self.arrayData = res?.results
                            
                            self.tableView.reloadData()
                        }
                    }
                }
            })
            
            task.resume()
        }
        
    }
    
}

extension ViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayData?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArtistTableViewCell", for: indexPath) as! ArtistTableViewCell
        
        let current = self.arrayData?[indexPath.row]
        cell.lblArtistName.text = current?.artistName
        cell.lblSubtitle.text = current?.collectionName
        
        let url = URL(string: current?.artworkUrl100 ?? "")!
        
        cell.imgView.load(url: url)
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let next = self.storyboard?.instantiateViewController(withIdentifier: "ArtistDetailsViewController") as! ArtistDetailsViewController
        next.res = arrayData?[indexPath.row]
        self.navigationController?.pushViewController(next, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 102
    }
    
}


extension ViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        callAPi(artist: searchBar.text ?? "")
        searchBar.text = ""
        self.view.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        self.view.endEditing(true)
    }
    
    
}
