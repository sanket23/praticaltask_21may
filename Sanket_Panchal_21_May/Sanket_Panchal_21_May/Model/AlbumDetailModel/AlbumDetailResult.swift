//
//  Result.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 21, 2021

import Foundation

struct AlbumDetailResult : Codable {

        let amgArtistId : Int?
        let artistId : Int?
        let artistLinkUrl : String?
        let artistName : String?
        let artistType : String?
        let primaryGenreId : Int?
        let primaryGenreName : String?
        let wrapperType : String?

        enum CodingKeys: String, CodingKey {
                case amgArtistId = "amgArtistId"
                case artistId = "artistId"
                case artistLinkUrl = "artistLinkUrl"
                case artistName = "artistName"
                case artistType = "artistType"
                case primaryGenreId = "primaryGenreId"
                case primaryGenreName = "primaryGenreName"
                case wrapperType = "wrapperType"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                amgArtistId = try values.decodeIfPresent(Int.self, forKey: .amgArtistId)
                artistId = try values.decodeIfPresent(Int.self, forKey: .artistId)
                artistLinkUrl = try values.decodeIfPresent(String.self, forKey: .artistLinkUrl)
                artistName = try values.decodeIfPresent(String.self, forKey: .artistName)
                artistType = try values.decodeIfPresent(String.self, forKey: .artistType)
                primaryGenreId = try values.decodeIfPresent(Int.self, forKey: .primaryGenreId)
                primaryGenreName = try values.decodeIfPresent(String.self, forKey: .primaryGenreName)
                wrapperType = try values.decodeIfPresent(String.self, forKey: .wrapperType)
        }

}
