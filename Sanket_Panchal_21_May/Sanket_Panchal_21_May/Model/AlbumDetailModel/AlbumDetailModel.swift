//
//  AlbumDetailModel.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on May 21, 2021

import Foundation

struct AlbumDetailModel : Codable {

        let resultCount : Int?
        let results : [AlbumDetailResult]?

        enum CodingKeys: String, CodingKey {
                case resultCount = "resultCount"
                case results = "results"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                resultCount = try values.decodeIfPresent(Int.self, forKey: .resultCount)
                results = try values.decodeIfPresent([AlbumDetailResult].self, forKey: .results)
        }

}
