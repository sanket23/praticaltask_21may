//
//  ArtistDetailsViewController.swift
//  Sanket_Panchal_21_May
//
//  Created by Unity on 5/21/21.
//  Copyright © 2021 Unity. All rights reserved.
//

import UIKit

class ArtistDetailsViewController: UIViewController {

    @IBOutlet weak var imgTrake: UIImageView!
    @IBOutlet weak var lblTrakeTitle: UILabel!
    @IBOutlet weak var lblArtistName: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    
    var res : Result?
    var albulDetail : AlbumDetailResult?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string: res?.artworkUrl100 ?? "")!
        imgTrake.load(url: url)
        lblArtistName.text = res?.artistName
        lblTrakeTitle.text = res?.trackName
        callAPi(trakeId: "\(res?.artistId ?? 0)")
        
        
        
    }
    

    @IBAction func btnTrakeAction(_ sender: Any) {
       
        let next = self.storyboard?.instantiateViewController(withIdentifier: "mapViewController") as! mapViewController
        next.strUrl = albulDetail?.artistLinkUrl ?? ""
        self.navigationController?.pushViewController(next, animated: true)
        
    }
    
    func callAPi(trakeId: String) {
        
        if trakeId == "" { return }
        DispatchQueue.global().async { //(https://itunes.apple.com/search?term=arijit)
            // Details using this api
            //            (https://itunes.apple.com/lookup?id=909253)
            
            guard let url = URL(string: "https://itunes.apple.com/lookup?id=\(trakeId)") else { return  }
            
            let task = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                
                if error != nil {
                    print(error?.localizedDescription as Any)
                    alertView(msg: error?.localizedDescription ?? "", vc: self)
                    return
                }
                
                do {
                    DispatchQueue.main.async {
                        do {
                            let res = try? JSONDecoder().decode(AlbumDetailModel.self, from: data!)
                            
                            if res?.resultCount ?? 0 == 0 {
                                alertView(msg: "No result Found", vc: self)
                                return
                            }
                            
                            self.albulDetail = res?.results?.first
                           
                        }
                    }
                }
            })
            
            task.resume()
        }
        
    }
    
}
