//
//  extention.swift
//  Sanket_Panchal_21_May
//
//  Created by Unity on 5/21/21.
//  Copyright © 2021 Unity. All rights reserved.
//

import Foundation
import  UIKit


extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}

func alertView(msg: String, vc: UIViewController) {
    let alert = UIAlertController(title: "", message: msg, preferredStyle: .alert)
    let ok = UIAlertAction(title: "Ok", style: .default) { (act) in
        
    }
    alert.addAction(ok)
    vc.present(alert, animated: true, completion: nil)
}
