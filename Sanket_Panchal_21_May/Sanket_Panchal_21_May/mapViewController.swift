//
//  mapViewController.swift
//  Sanket_Panchal_21_May
//
//  Created by Unity on 5/21/21.
//  Copyright © 2021 Unity. All rights reserved.
//

import UIKit
import WebKit

class mapViewController: UIViewController {

    
    @IBOutlet weak var tempView: UIView!
    
    private let webView = WKWebView()
    var strUrl = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        webView.translatesAutoresizingMaskIntoConstraints = false
        self.tempView.addSubview(self.webView)
        // You can set constant space for Left, Right, Top and Bottom Anchors
        NSLayoutConstraint.activate([
            self.webView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            self.webView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            self.webView.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            self.webView.topAnchor.constraint(equalTo: self.view.topAnchor),
            ])
        // For constant height use the below constraint and set your height constant and remove either top or bottom constraint
        //self.webView.heightAnchor.constraint(equalToConstant: 200.0),
        
        self.view.setNeedsLayout()
        
        
        if strUrl == "" { return }
        let request = URLRequest(url: URL.init(string: strUrl)!)
        self.webView.load(request)
    }
    


}
